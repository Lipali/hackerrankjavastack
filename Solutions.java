import java.util.*;
class Solution {

    public static void main(String[] argh) {
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()) {
            int flag =0 ;
            String input = sc.next();
            Stack<Character> stack = new Stack<>();

            if (input.length()%2 != 0){
                flag++;
            }

            for (int i = 0; i < input.length(); i++) {
                if (input.charAt(i) == '(')
                    stack.push('(');
                else if (input.charAt(i) == '{')
                    stack.push('{');
                else if (input.charAt(i) == '[')
                    stack.push('[');
                else if (input.charAt(i) == ')') {
                    if (stack.isEmpty() || (stack.pop().charValue() != '(')) {
                        flag ++;
                    }
                } else if (input.charAt(i) == '}') {
                    if (  stack.isEmpty()  ||  stack.pop().charValue() != '{' ) {
                        flag ++;
                    }

                } else if (input.charAt(i) == ']') {
                    if (stack.isEmpty() || (stack.pop().charValue() != '[')) {
                        flag ++;
                    }
                }
            }
            if (flag == 0) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }
        }
    }
}


